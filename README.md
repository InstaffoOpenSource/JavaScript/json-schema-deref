<!--#echo json="package.json" key="name" underline="=" -->

# @instaffogmbh/json-schema-deref

<!--/#echo -->

<!--#echo json="package.json" key="description" -->

Resolves references in JSON schema files to combine them into one file.

<!--/#echo -->

- 📣 💼 💰 Looking for a tech job?
  Try our [reverse recruiting service](https://instaffo.com/).

## Usage

CLI: `npx @instaffogmbh/json-schema-deref input.json output.json`

## Libraries

See: https://www.npmjs.com/package/json-schema-ref-parser

## License

<!--#echo json="package.json" key=".license" -->

MIT

<!--/#echo -->
