#!/usr/bin/env node

const $RefParser = require("@apidevtools/json-schema-ref-parser");
const fs = require("fs");

if (process.argv.length < 4) {
  console.log("Usage: npx @instaffogmbh/json-schema-deref INPUT OUTPUT");
  process.exit(1);
}

const [_1, _2, input, output] = process.argv;

fs.readFile(input, "utf8", function(err, data) {
  const schema = JSON.parse(data);

  $RefParser.dereference(schema, (err, schema) => {
    if (err) {
      console.error(err);
    } else {
      fs.writeFile(output, JSON.stringify(schema, null, 2), "utf8", () => null);
    }
  });
});
